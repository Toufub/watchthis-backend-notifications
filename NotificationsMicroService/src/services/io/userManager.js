class UserManager {
    userMapper = null;

    constructor(userMapper) {
        this.userMapper = userMapper;
    }

    /**
     *
     * Initialisation de l'utilisateur sur le Socket
     * (Authentification)
     *
     */

    send_login(socket, login) {
        if(this.userMapper.add_user(login, socket.id)) {
            socket.emit("confirmation-message", "ok");
            console.log(login.toString() + " join.");
        } else {
            socket.emit("forbidden", "already-use");
            socket.disconnect();
        }
    }

    /**
     *
     * Réactualise l'ID du socket par rapport à son login
     *
     */

    fix_id(socket, login) {
        if(this.userMapper.fix_socket_id(login, socket.id)) {
            socket.login = login;
            console.log(login.toString() + " fixed.");
        } else {
            socket.emit("forbidden", "Not connected !");
            socket.disconnect();
        }
    }


}

module.exports = UserManager;