// private services
const UserMapper = require('./userMapper.js');
const UserManager = require('./io/userManager.js');

const userMapper = new UserMapper();
const userManager = new UserManager(userMapper);

class IoManager {

    io = null;

    constructor(io) {
        this.io = io;
    }

    init() {
        this.io.on('connection', function (socket) {
			
			socket.on("user-login", function (data) {
					try {
						userManager.send_login(socket, parseInt(data));
					} catch (e) {
						"l'id n'est pas un INT"
					}
			});
			
			socket.on("fix-id", function (data) {
					try {
						userManager.fix_id(socket, parseInt(data));
					} catch (e) {
						"l'id n'est pas un INT"
					}
			});

            // impossible de mettre dans le onAny
            socket.on('disconnect', () => {
                if (userMapper.check_user_connection(socket.login)) {
                    userMapper.remove_user(socket.login);
                    console.log(socket.login + " left.");
                    socket.broadcast.emit("disconnect-event", socket.login);
                }
            });
        });
    }

	send_message_to_user(utilisateur_id, type, data) {
		var socket_id = userMapper.get_socket_id(utilisateur_id);
		if(socket_id) {
			this.io.to(socket_id).emit(type, data);
			return true;
		} else {
			return false;
		}
	}
}
module.exports = IoManager;