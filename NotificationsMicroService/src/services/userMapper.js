class UserMapper {

    logins_connected = [];
    logins_sessions = [];

    constructor() {

    }

    get_all_logins_connected() {
        return this.logins_connected;
    }

    fix_socket_id(login, id) {
        let val_retour = false;
        if(this.check_user_connection(login) === true) {
            val_retour = true;
            let index = this.logins_sessions.findIndex(elem => elem.login === login);
            this.logins_sessions[index].session = id;
        }
        return val_retour;
    }

    get_socket_id(login) {
        return this.logins_sessions.find(obj => {
            return obj.login === login;
        })?.socket;
    }

    get_user_login(socket) {
        return this.logins_sessions.find(obj => {
            return obj.socket === socket;
        })?.login;
    }

    check_user_connection(login) {
        return this.logins_connected.includes(login);
    }

    add_user(login,socketId) {
        let success = false;
        if(this.logins_connected.includes(login) === false) {
            this.logins_connected.push(login);
            this.logins_sessions.push({login: login,socket:socketId});
            success = true;
        }
        return success;
    }

    remove_user(login) {
        const found = this.logins_sessions.findIndex(element => element?.login === login);
        this.logins_connected.splice(this.logins_connected.indexOf(login), 1);
        delete this.logins_sessions[found];
    }

}

module.exports = UserMapper;