const port_ecoute_node = require("./settings.js").port_ecoute_serveur_node;

const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const IoManager = require("./services/ioManager.js");

const amqp = require('amqplib/callback_api');

const ioManager = new IoManager(
  new Server(server, {
    cors: {
      origin: ["http://localhost", "http://localhost:3000", "http://89.86.95.28"],
      allowedHeaders: ["my-custom-header"],
    },
  })
);

const cors = require("cors");
app.use(
  cors({
      origin: ["http://localhost", "http://localhost:3000", "http://89.86.95.28"],
  })
);

ioManager.init();

server.listen(port_ecoute_node, () => {
  console.log("Node listening on *:" + port_ecoute_node);
});

app.get('/', (req, res) => {
	res.status(200).send("OK")
});

amqp.connect('amqp://rabbitmq', function(error0, connection) {
	console.log("Connected to rabbitmq");
	connection.createChannel(function(error1, channel) {
		if (error1) {
			throw error1;
		}
		var queue = 'films_recommandes';

		channel.assertQueue(queue, {
			durable: false
		});
		channel.consume(queue, function(msg) {
			dataStr = msg.content.toString();
			console.log(dataStr);
			try {
				let data = JSON.parse(dataStr);
				if(ioManager.send_message_to_user(parseInt(data.idUtilisateur), "movies_predict", data.filmsPredict)) {
					console.log("Données transmises à l'utilisateur (connecté)");					
				} else {
					console.log("Données non transmises à l'utilisateur (déconnecté)");
				}
			} catch (e) {
				console.log("Les data ne sont pas formattées");
			}
		}, {
			noAck: true
		}); 
    });
});
